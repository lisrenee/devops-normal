package com.jenkins.practica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.jenkins.utils.DogsOperations;

public class OperationsTests {

	/**
	 * Checks if the operation getRandomDogImage returns a .jpg a
	 */
	@Test
	public void testRandom() {
		// Instantiate DogsOperations class
		DogsOperations c= new DogsOperations();
		// Call getRandomDogImage operation and store the result on String
		String m=c.getRandomDogImage(); 
		assertTrue(m.contains(".jpg")); 
		// Assert true if the result string ends with '.jpg'  
	}
	
	/**
	 * Checks if the operation getBreedList returns a list of dogs (f.e. size > 0)
	 */
	@Test
	public void breedsList() {
		// Instantiate DogsOperations class
		DogsOperations c= new DogsOperations();
		// Call getBreedList operation and store the result on ArrayList
		ArrayList m=c.getBreedList();
		assertTrue(m.size()>0);
		// Assert true if the result ArrayList has size of more than 0
	}
}
